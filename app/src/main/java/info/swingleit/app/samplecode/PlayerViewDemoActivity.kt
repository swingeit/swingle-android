/*
 * Copyright 2012 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package info.swingleit.app.samplecode

import android.os.Bundle
import android.util.Log
import com.google.android.youtube.player.YouTubeBaseActivity
import com.google.android.youtube.player.YouTubeInitializationResult
import com.google.android.youtube.player.YouTubePlayer
import com.google.android.youtube.player.YouTubePlayerView
import info.swingleit.app.R
import kotlinx.android.synthetic.main.playerview_demo.*

/**
 * A simple YouTube Android API demo application which shows how to create a simple application that
 * displays a YouTube Video in a [YouTubePlayerView].
 *
 *
 * Note, to use a [YouTubePlayerView], your activity must extend [YouTubeBaseActivity].
 */
class PlayerViewDemoActivity : YouTubeBaseActivity(), YouTubePlayer.OnInitializedListener {


    val DEVELOPER_KEY = "AIzaSyBUQbZAsNdeMYxXyz3jMP5fIjfc-BLfNn8"
    private val VIDEO_ID = "VIDEO_ID"
    var videoId: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.playerview_demo)
        youtube_view.initialize(DEVELOPER_KEY, this)
        videoId = intent.extras?.getString("videoId")
        Log.i(VIDEO_ID, videoId)
    }

    override fun onInitializationSuccess(provider: YouTubePlayer.Provider,
                                         player: YouTubePlayer,
                                         wasRestored: Boolean) {
        if (!wasRestored) {
            player.cueVideo(videoId)
        }
    }

    override fun onInitializationFailure(provider: YouTubePlayer.Provider?,
                                         result: YouTubeInitializationResult?) {

    }
}
