package info.swingleit.app.profile

import android.app.Activity
import android.app.Fragment
import android.content.Intent
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.sendbird.android.SendBird
import com.sendbird.android.User
import com.squareup.picasso.Picasso
import info.swingleit.app.R
import info.swingleit.app.util.FileStorageUtil
import info.swingleit.app.util.UserUtil
import kotlinx.android.synthetic.main.fragment_profile.*
import java.io.File


class UserProfileFragment : Fragment() {

    private val READ_REQUEST_CODE: Int = 34232

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupUser()

        flProfileImage.setOnClickListener {
            showImagePicker()
        }
    }

    private fun setupUser() {
        val user: User? = SendBird.getCurrentUser()
        etFullName.setText(UserUtil.getUserName())
        Log.i("setupuser", user?.profileUrl)
//        val bitmap = BitmapFactory.decodeFile(UserUtil.getProfileImagePath())
//        civProfileImage.setImageBitmap(bitmap)
        Picasso.get().load(user?.profileUrl).into(civProfileImage)
    }

    private fun showImagePicker() {
        // ACTION_OPEN_DOCUMENT is the intent to choose a file via the system's file
        // browser.
        val intent = Intent(Intent.ACTION_OPEN_DOCUMENT)

        // Filter to only show results that can be "opened", such as a
        // file (as opposed to a list of contacts or timezones)
        intent.addCategory(Intent.CATEGORY_OPENABLE)

        // Filter to show only images, using the image MIME data type.
        // If one wanted to search for ogg vorbis files, the type would be "audio/ogg".
        // To search for all documents available via installed storage providers,
        // it would be "*/*".
        intent.type = "image/*"

        startActivityForResult(intent, READ_REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == READ_REQUEST_CODE
                && resultCode == Activity.RESULT_OK) {
            Log.i("onActivityResult", "got data")
            val uri: Uri? = data?.data
            if (uri != null) {
                val imagePath = FileStorageUtil.savePictureAndGetFilePath(activity, uri)
                Log.i("onActivityResult", "path " + imagePath)
                UserUtil.saveProfileImagePath(imagePath!!)

                val bitmap = BitmapFactory.decodeFile(UserUtil.getProfileImagePath())
                civProfileImage.setImageBitmap(bitmap)
                syncProfileImage()
            }
        }
    }

    private fun syncProfileImage() {
        Log.i("syncProfileImage", "called")
        val userName = UserUtil.getUserName()
        val profileImage = File(UserUtil.getProfileImagePath())

        SendBird.connect(UserUtil.getUserId()) { user, e ->
            if (e != null) {
                e.printStackTrace()
                return@connect
            }

            SendBird.updateCurrentUserInfoWithProfileImage(userName, profileImage) { e ->
                if (e != null) {
                    Log.i("syncProfileImage", "syncProfileImage failed")
                    e.printStackTrace()
                    return@updateCurrentUserInfoWithProfileImage
                }

                Log.i("syncProfileImage", "syncProfileImage success")
            }
        }
    }
}
