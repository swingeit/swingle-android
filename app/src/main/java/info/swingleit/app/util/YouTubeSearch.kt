package info.swingleit.app.util

import com.google.api.client.http.HttpRequestInitializer
import com.google.api.client.http.HttpTransport
import com.google.api.client.http.javanet.NetHttpTransport
import com.google.api.client.json.JsonFactory
import com.google.api.client.json.jackson2.JacksonFactory
import com.google.api.services.youtube.YouTube
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable


class YouTubeSearch {

    fun searchVidById(vidId: String?): HashMap<String, String>? {
        vidId ?: return null

        val vidResult: HashMap<String, String> = HashMap()
        val youtube = YouTube.Builder(Auth.HTTP_TRANSPORT,
                Auth.JSON_FACTORY,
                HttpRequestInitializer {

                }).setApplicationName("youtube-cmdline-search-sample").build()

        val search = youtube.videos().list("id,contentDetails,snippet")

        // Set your developer key from the {{ Google Cloud Console }} for
        // non-authenticated requests. See:
        // {{ https://cloud.google.com/console }}
        val apiKey = "AIzaSyA9ZXOKJNiQnR4VMe5KuyjSZVzwUzQIHMg"
        search.key = apiKey
        search.set("id", vidId)

        // Restrict the search results to only include videos. See:
        // https://developers.google.com/youtube/v3/docs/search/list#type
//        search.setType("video")

        // To increase efficiency, only retrieve the fields that the
        // application uses.
        search.setFields("items(snippet(title,description,channelTitle),contentDetails(duration))")
        search.setMaxResults(1)

        // Call the API and print results.
        val searchResponse = search.execute()
        val searchResultList = searchResponse.items
        if (searchResultList != null) {
            searchResultList.forEach { result ->
                vidResult.put("channelTitle", result.snippet.channelTitle)
                vidResult.put("title", result.snippet.title)
                vidResult.put("description", result.snippet.description)
                vidResult.put("duration", result.contentDetails.duration)
            }
        }

        return vidResult
    }

    fun searchVidByIdAsync(vidId: String): Flowable<HashMap<String, String>> {
        return Flowable.create({ emitter ->
            var result = HashMap<String, String>()
            val vid = searchVidById(vidId)
            if (vid != null) result = vid

            emitter.onNext(result)
        }, BackpressureStrategy.BUFFER)
    }
}


/**
 * Shared class used by every sample. Contains methods for authorizing a user and caching credentials.
 */
object Auth {

    /**
     * Define a global instance of the HTTP transport.
     */
    val HTTP_TRANSPORT: HttpTransport = NetHttpTransport()

    /**
     * Define a global instance of the JSON factory.
     */
    val JSON_FACTORY: JsonFactory = JacksonFactory()

    /**
     * This is the directory that will be used under the user's home directory where OAuth tokens will be stored.
     */
    private val CREDENTIALS_DIRECTORY = ".oauth-credentials"
}