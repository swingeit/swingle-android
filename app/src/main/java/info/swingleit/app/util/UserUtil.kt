package info.swingleit.app.util

class UserUtil {

    companion object {

        private val userNameKey = "userNameKey"
        private val userIdKey = "userIdKey"
        private val fcmTokenKey = "fcmTokenKey"
        private val profileImageKey = "profileImageKey"
        private val profileImageUrlKey = "profileImageUrlKey"

        fun getUserName(): String? {
            return KeyValueStore().get(userNameKey, null) as? String
        }

        fun saveUserName(username: String) {
            KeyValueStore().put(userNameKey, username)
        }

        fun getUserId(): String? {
            return KeyValueStore().get(userIdKey, null) as? String
        }

        fun saveUserId(userId: String) {
            KeyValueStore().put(userIdKey, userId)
        }

        fun getProfileImagePath(): String? {
            return KeyValueStore().get(profileImageKey, null) as? String
        }

        fun saveProfileImagePath(profileImagePath: String) {
            KeyValueStore().put(profileImageKey, profileImagePath)
        }

        fun getProfileImageUrl(): String? {
            return KeyValueStore().get(profileImageUrlKey, null) as String?
        }

        fun saveProfileImageUrl(profileImageUrl: String) {
            KeyValueStore().put(profileImageUrlKey, profileImageUrl)
        }

        fun saveFcmToken(fcmToken: String) {
            KeyValueStore().put(fcmTokenKey, fcmToken)
        }

        fun getFcmToken(): String? {
            return KeyValueStore().get(fcmTokenKey, null) as String
        }
    }
}