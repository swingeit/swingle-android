package info.swingleit.app.util

class CategoryManager {

    companion object {

        val VIRAL_AND_TRENDING = "Viral & Trending"
        val PETS_AND_ANIMALS = "Pets & Animals"
        val STYLE_AND_FASHION = "Style & Fashion"
        val TARVEL_AND_DESTINATION = " Travel & Destinations"
        val SCIENCE_TECH = "Science & Tech"
        val FINANCE = "Finance & Commerce"
        val ART = "Art & Paintings"
        val COMEDY = "Comedy & Humour"
        val FAMILY = "Family & Parenting"
        val MOVIES = "Movie clips & Trailers"
        val SONGS = "Songs & Covers"
        val TV_SHOWS = "TV Show clips"
        val EDUCATION = "Education & Motivation"
        val RELEGION = "Religion & Faith"
        val NEWS = "News & Politics"
        val VLOGS = "Vlogs & Podcast"
        val FOOD = "Food & Cooking"
        val FITNESS = "Fitness & Health"
        val AUTOMOTIVE = "Automotive"
        val SPORTS = "Sports"
        val RELATIONSHIPS = "Relationships & Dating"
        val PROD_REVIEW = "Product Review & Unboxing"
        val GAMING = "Gaming"
        val PRANKS = "Pranks"
        val DIFFERENT = "Something Different"


        fun getAllCategories(): MutableList<String> {
            val categories: MutableList<String> = ArrayList()

            categories.add(VIRAL_AND_TRENDING)
            categories.add(PETS_AND_ANIMALS)
            categories.add(STYLE_AND_FASHION)
            categories.add(TARVEL_AND_DESTINATION)
            categories.add(SCIENCE_TECH)
            categories.add(FINANCE)
            categories.add(ART)
            categories.add(COMEDY)
            categories.add(FAMILY)
            categories.add(MOVIES)
            categories.add(SONGS)
            categories.add(TV_SHOWS)
            categories.add(EDUCATION)
            categories.add(RELEGION)
            categories.add(NEWS)
            categories.add(VLOGS)
            categories.add(FOOD)
            categories.add(FITNESS)
            categories.add(AUTOMOTIVE)
            categories.add(SPORTS)
            categories.add(RELATIONSHIPS)
            categories.add(PROD_REVIEW)
            categories.add(GAMING)
            categories.add(PRANKS)
            categories.add(DIFFERENT)

            return categories
        }
    }
}