package info.swingleit.app.util

import android.content.Context
import android.graphics.Bitmap
import android.net.Uri
import android.os.Environment
import java.io.File
import android.graphics.BitmapFactory
import android.util.Log
import java.io.FileOutputStream
import java.text.SimpleDateFormat
import java.util.*


class FileStorageUtil {

    companion object {

        val dateFormat = "MM_dd_yyyy_hh_mm_ss"
        var storageDir: String = ""

        fun init(context: Context) {
            // Permission to read write to external dir

            val appDir = "/" + context.packageName
            storageDir =
                    Environment.getExternalStorageDirectory().path + appDir
            val file = File(storageDir)
            file.mkdirs()
        }

        fun isStorageDirExists(): Boolean {
            return !storageDir.isEmpty() && File(storageDir).exists()
        }

        fun savePictureAndGetFilePath(context: Context, uri: Uri): String? {
            if (uri == Uri.EMPTY) return null

            val inputStream = context.contentResolver.openInputStream(uri)
            val bitmap = BitmapFactory.decodeStream(inputStream)
            inputStream.close()

            val sdf = SimpleDateFormat(dateFormat)
            val file = File(storageDir, sdf.format(Date()) + ".png")
            file.createNewFile()
            val fOut = FileOutputStream(file)

            // 100 means no compression, the lower you go, the stronger the compression
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fOut)
            fOut.flush()
            fOut.close()
            return file.path
        }
    }
}