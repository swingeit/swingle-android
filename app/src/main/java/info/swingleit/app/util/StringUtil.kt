package info.swingleit.app.util

import org.apache.commons.codec.binary.Hex
import org.apache.commons.codec.digest.DigestUtils
import java.util.regex.Pattern

const val youtubePattern = "^https?://.*(?:youtu.be/|v/|u/\\w/|embed/|watch?v=)([^#&?]*).*$"

fun String.Companion.cleanPhoneNumber(input: String): String? {
    var phone = input.replace(" ", "")
    if (phone.length > 10) {
        phone = phone.substring(phone.length - 10)
    }

    return phone
}

fun String.Companion.getSaltString(input: String?): String? {
    if (input?.length == 0) return input
    return String(Hex.encodeHex(DigestUtils.md5(input)))
}

fun String.Companion.getYTVideoId(ytUrl: String?): String? {
    ytUrl ?: return null
    var vId: String? = null
    val pattern = Pattern.compile(youtubePattern, Pattern.CASE_INSENSITIVE)
    val matcher = pattern.matcher(ytUrl)
    if (matcher.matches()) vId = matcher.group(1)
    return vId
}

fun String.isValidYouTubeUrl(): Boolean {
    return matches(Regex(youtubePattern))
}

fun String.Companion.getYTThumbnailUrl(ytUrl: String?): String? {
    if (ytUrl == null) return null

    val ytId = String.getYTVideoId(ytUrl)
    return "http://img.youtube.com/vi/$ytId/sddefault.jpg"
}