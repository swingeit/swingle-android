package info.swingleit.app.util

import android.content.Context
import com.orhanobut.hawk.Hawk

class KeyValueStore : IKeyValueStore {

    override fun init(context: Context) {
        Hawk.init(context).build()
    }

    override fun put(key: String, data: Any) {
        Hawk.put(key, data)
    }

    override fun get(key: String, default: Any?): Any? {
        return Hawk.get(key, default)
    }
}

interface IKeyValueStore {

    fun init(context: Context)

    fun put(key: String, data: Any)

    fun get(key: String, default: Any?): Any?
}