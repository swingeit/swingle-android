package info.swingleit.app.util

import android.content.Context
import android.support.v4.os.ConfigurationCompat
import android.text.TextUtils
import com.github.marlonlom.utilities.timeago.TimeAgo
import com.github.marlonlom.utilities.timeago.TimeAgoMessages
import java.text.SimpleDateFormat
import java.util.*


class DateUtil {
    companion object {
        // 2017-07-20T23:23:50Z
        val FORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'"

        fun parseDuration(input: String): String {
            var duration = input
            duration = if (duration.contains("PT")) duration.replace("PT", "") else duration
            duration = if (duration.contains("S")) duration.replace("S", "") else duration
            duration = if (duration.contains("H")) duration.replace("H", ":") else duration
            duration = if (duration.contains("M")) duration.replace("M", ":") else duration
            val split = duration.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            for (i in split.indices) {
                val item = split[i]
                split[i] = if (item.length <= 1) "0$item" else item
            }

            return TextUtils.join(":", split)
        }

        fun getFormattedCurDate(): String {
            val sdf = SimpleDateFormat(FORMAT)
            return sdf.format(Date())
        }

        fun getRelativeTime(date: String, context: Context): String {
            val sdf = SimpleDateFormat(FORMAT)
            val locale = ConfigurationCompat.getLocales(context.resources.configuration).get(0)
            val messages = TimeAgoMessages.Builder().withLocale(locale).build()
            return TimeAgo.using(sdf.parse(date).time, messages)
        }

        fun getRelativeTime(date: Date, context: Context): String {
            val locale = ConfigurationCompat.getLocales(context.resources.configuration).get(0)
            val messages = TimeAgoMessages.Builder().withLocale(locale).build()
            return TimeAgo.using(date.time, messages)
        }
    }
}