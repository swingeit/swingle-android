package info.swingleit.app.videoplayer

import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.SeekBar
import android.widget.TextView
import android.widget.Toast
import com.google.android.youtube.player.YouTubeBaseActivity
import com.google.android.youtube.player.YouTubeInitializationResult
import com.google.android.youtube.player.YouTubePlayer
import com.google.android.youtube.player.YouTubePlayer.*
import com.google.android.youtube.player.YouTubePlayerView
import info.swingleit.app.R
import kotlinx.android.synthetic.main.activity_custom_player.*

class VideoPlayerActivity : YouTubeBaseActivity(), OnInitializedListener, View.OnClickListener {

    private var mPlayer: YouTubePlayer? = null

    private var mPlayButtonLayout: View? = null
    private var mPlayTimeTextView: TextView? = null

    private var mHandler: Handler? = null
    private var mSeekBar: SeekBar? = null

    private var mPlaybackEventListener: PlaybackEventListener = object : PlaybackEventListener {
        override fun onBuffering(arg0: Boolean) {}

        override fun onPaused() {
            mHandler!!.removeCallbacks(runnable)
        }

        override fun onPlaying() {
            mHandler!!.postDelayed(runnable, 100)
            displayCurrentTime()
        }

        override fun onSeekTo(arg0: Int) {
            mHandler!!.postDelayed(runnable, 100)
        }

        override fun onStopped() {
            mHandler!!.removeCallbacks(runnable)
        }
    }

    private var mPlayerStateChangeListener: PlayerStateChangeListener = object : PlayerStateChangeListener {
        override fun onAdStarted() {}

        override fun onError(arg0: ErrorReason) {}

        override fun onLoaded(arg0: String) {}

        override fun onLoading() {}

        override fun onVideoEnded() {}

        override fun onVideoStarted() {
            displayCurrentTime()
        }
    }

    private var mVideoSeekBarChangeListener: SeekBar.OnSeekBarChangeListener = object : SeekBar.OnSeekBarChangeListener {
        override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
            val lengthPlayed = (mPlayer!!.durationMillis * progress / 100).toLong()
            mPlayer!!.seekToMillis(lengthPlayed.toInt())
        }

        override fun onStartTrackingTouch(seekBar: SeekBar) {

        }

        override fun onStopTrackingTouch(seekBar: SeekBar) {

        }
    }

    private val runnable = object : Runnable {
        override fun run() {
            displayCurrentTime()
            mHandler!!.postDelayed(this, 100)
        }
    }

//    private var isPlaying: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // attaching layout xml
        setContentView(R.layout.activity_custom_player)

        // Initializing YouTube player view
        val youTubePlayerView = findViewById<View>(R.id.youtube_player_view) as YouTubePlayerView
        youTubePlayerView.initialize(API_KEY, this)

        //Add play button to explicitly play video in YouTubePlayerView
        mPlayButtonLayout = findViewById(R.id.video_control)
        findViewById<View>(R.id.play_video).setOnClickListener(this)

        mPlayTimeTextView = findViewById<View>(R.id.play_time) as TextView
        mSeekBar = findViewById<View>(R.id.video_seekbar) as SeekBar
        mSeekBar!!.setOnSeekBarChangeListener(mVideoSeekBarChangeListener)

        mHandler = Handler()
    }

    override fun onInitializationFailure(provider: Provider, result: YouTubeInitializationResult) {
        Toast.makeText(this, "Failed to initialize.", Toast.LENGTH_LONG).show()
    }

    override fun onInitializationSuccess(provider: Provider, player: YouTubePlayer?, wasRestored: Boolean) {
        if (null == player) return
        mPlayer = player

        displayCurrentTime()

        // Start buffering
        if (!wasRestored) {
            player.loadVideo(intent?.extras?.getString(videoId))
        }

        player.setPlayerStyle(PlayerStyle.CHROMELESS)
        mPlayButtonLayout!!.visibility = View.VISIBLE

        // Add listeners to YouTubePlayer instance
        player.setPlayerStateChangeListener(mPlayerStateChangeListener)
        player.setPlaybackEventListener(mPlaybackEventListener)

    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.play_video -> {
                if (null != mPlayer) {
                    if (!mPlayer!!.isPlaying) {
                        mPlayer!!.play()
                        play_video.setImageResource(R.drawable.ic_play)
                    } else {
                        mPlayer!!.pause()
                        play_video.setImageResource(R.drawable.ic_pause)
                    }
                }
            }
        }
    }

    private fun displayCurrentTime() {
        if (null == mPlayer) return
        val formattedTime = formatTime(mPlayer!!.durationMillis - mPlayer!!.currentTimeMillis)
        mPlayTimeTextView!!.text = formattedTime
    }

    private fun formatTime(millis: Int): String {
        val seconds = millis / 1000
        val minutes = seconds / 60
        val hours = minutes / 60

        return (if (hours == 0) "--:" else hours.toString() + ":") + String.format("%02d:%02d", minutes % 60, seconds % 60)
    }

    companion object {
        private val TAG = VideoPlayerActivity::class.java.simpleName

        val API_KEY = "AIzaSyBUQbZAsNdeMYxXyz3jMP5fIjfc-BLfNn8"
        val videoId = "videoId"
    }

}