package info.swingleit.app

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import info.swingleit.app.login.LoginActivity
import info.swingleit.app.util.UserUtil
import kotlinx.android.synthetic.main.activity_base.view.*

open class BaseActivity : AppCompatActivity() {

    val context = this

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun setContentView(layoutResID: Int) {
        val baseLayout = LayoutInflater.from(this).inflate(R.layout.activity_base, null)
        setContentView(baseLayout)
        setSupportActionBar(baseLayout.toolbar)
        baseLayout.viewStub.layoutResource = layoutResID
        baseLayout.viewStub.inflate()
    }

    fun isUserLoggedIn(): Boolean {
        val isLoggedIn = UserUtil.getUserId()?.isNotEmpty()

        if (isLoggedIn == null) {
            return false
        } else {
            return isLoggedIn
        }
    }

    /*
    * Override and set to False for non-session activities like
    * Login, OTP etc
    * */
    open fun isSessionActivity(): Boolean {
        return true
    }

    override fun onResume() {
        super.onResume()
        checkUserLoggedIn()
    }

    private fun checkUserLoggedIn() {
        if (isSessionActivity() && !isUserLoggedIn()) {
            val intent = Intent(this, LoginActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(intent)
            finish()
        }
    }
}
