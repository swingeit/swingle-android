package info.swingleit.app.chat

import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.github.tamir7.contacts.Contact
import com.github.tamir7.contacts.Contacts
import info.swingleit.app.R
import info.swingleit.app.data.MyAuthor
import info.swingleit.app.util.cleanPhoneNumber
import kotlinx.android.synthetic.main.fragment_select_contacts.*

class SelectContactsFragment : Fragment(), SelectContactsListener {
    private var listener: CreateGroupListener? = null
    private lateinit var adapter: SelectContactsAdapter
    val title = "Select Contacts"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_select_contacts, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        listener = activity as? CreateGroupActivity

        // TODO give permissions
        val query = Contacts.getQuery()
        query.hasPhoneNumber()
        val contacts = query.find()
        adapter = SelectContactsAdapter(contacts, this, listener?.isMultiSelectAllowed()!!)
        rvContacts.adapter = adapter
        rvContacts.layoutManager = LinearLayoutManager(context) as RecyclerView.LayoutManager?

        if (!listener?.isMultiSelectAllowed()!!) tvShareWithEveryone.visibility = View.GONE

        tvShareWithEveryone.setOnClickListener {
            adapter.onNextPressed()
        }
    }

    override fun onContactsSelected(contacts: List<Contact>) {
        Log.i("select", "" + contacts.size)
        val authors: MutableList<MyAuthor> = ArrayList()
        var groupDetails = listener?.getGroupDetails()

        for (contact in contacts) {
            val phoneNumber =
                    String.cleanPhoneNumber(contact.phoneNumbers[0].number)
//            val userId = String.getSaltString(phoneNumber)!!
            val userId = phoneNumber
            Log.i("selected phone", " " + phoneNumber)
            Log.i("selected userId", " " + userId)
            authors.add(MyAuthor(userId!!, contact.displayName, contact.photoUri))
        }

        groupDetails?.users?.addAll(authors)

        if (!listener?.isMultiSelectAllowed()!!) {
            groupDetails?.name = contacts[0].displayName

            if (contacts[0].photoUri != null) {
                groupDetails?.groupImage = Uri.parse(contacts[0].photoUri)
            }
        } else {
            // TODO for mutliple contacts and group
        }

        listener?.setGroupDetails(groupDetails!!)
        listener?.onNextClicked()
    }
}
