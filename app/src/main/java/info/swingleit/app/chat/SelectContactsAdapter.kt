package info.swingleit.app.chat

import android.net.Uri
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import com.github.tamir7.contacts.Contact
import info.swingleit.app.R
import kotlinx.android.synthetic.main.item_contact.view.*

class SelectContactsAdapter(private var items: MutableList<Contact>,
                            private var listener: SelectContactsListener,
                            private var multiSelectAllowed: Boolean) :
        RecyclerView.Adapter<ViewHolder>() {

    private var selectedContacts: MutableList<Contact> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_contact, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return items.count()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val contact = items[position]

        if (multiSelectAllowed) {
            holder.itemView.checkBox.visibility = View.VISIBLE
        }

        holder.itemView.tvName.text = contact.displayName

        if (contact.photoUri != null) {
            holder.itemView.ivProfilePic.setImageURI(Uri.parse(contact.photoUri))
        } else {
            holder.itemView.ivProfilePic.setImageResource(R.drawable.blank_profile)
        }

        holder.itemView.setOnClickListener {
            handleClick(contact, holder.itemView.checkBox)
        }
    }

    private fun handleClick(contact: Contact, checkBox: CheckBox) {
        if (multiSelectAllowed) {
            // For groups
            if (selectedContacts.contains(contact)) {
                selectedContacts.remove(contact)
                checkBox.isChecked = false
            } else {
                checkBox.isChecked = true
                selectedContacts.add(contact)
            }

            Log.i("multiselect", "" + selectedContacts.size)
        } else {
            Log.i("multiselect not", "" + selectedContacts.size)
            selectedContacts.add(contact)
            listener.onContactsSelected(selectedContacts)
        }
    }

    fun onNextPressed() {
        listener.onContactsSelected(selectedContacts)
    }
}

class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

}

interface SelectContactsListener {

    fun onContactsSelected(contacts: List<Contact>)
}