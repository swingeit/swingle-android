package info.swingleit.app.chat

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import android.util.AttributeSet
import android.util.Log
import android.view.MenuItem
import android.view.MotionEvent
import com.sendbird.android.GroupChannel
import com.sendbird.android.GroupChannelParams
import info.swingleit.app.BaseActivity
import info.swingleit.app.R
import info.swingleit.app.chatroom.ChatRoomActivity
import info.swingleit.app.data.ContentVideo
import info.swingleit.app.util.FileStorageUtil
import info.swingleit.app.util.UserUtil
import kotlinx.android.synthetic.main.activity_create_group.*
import java.io.File
import java.util.*


class CreateGroupActivity : BaseActivity(), CreateGroupListener {

    val TAG = "CreateGroupActivity"
    val ALLOW_MULTIPLE: String = "ALLOW_MULTIPLE"
    private var allowMultiple: Boolean = false
    private val fragments: MutableList<Fragment> = ArrayList()
    private lateinit var pagerAdapter: FragmentPagerAdapter
    private var groupDetails: GroupDetails = GroupDetails()
    private var requestCode: Int? = 0
    private var contentVideo: ContentVideo? = null

    companion object {
        val contentVideoSTR = "contentVideo"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_group)
        setupToolbar()

        contentVideo = intent?.extras?.get(contentVideoSTR) as ContentVideo?
        allowMultiple = intent?.extras?.getBoolean(ALLOW_MULTIPLE) ?: false
        requestCode = intent?.extras?.getInt("REQ_CODE")
        startUserFlow()
    }

    private fun setupToolbar() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_back)
        updateToolbar(0)
    }

    private fun startUserFlow() {
        setupFragments(requestCode)
        pagerAdapter = MyPagerAdapter(supportFragmentManager, fragments)
        viewPager.adapter = pagerAdapter
    }

    override fun onBackPressed() {
        var position: Int = viewPager.currentItem

        if (position > 0) {
            viewPager.currentItem = position - 1
            updateToolbar(viewPager.currentItem)
        } else {
            super.onBackPressed()
            setResult(Activity.RESULT_CANCELED)
        }
    }

    private fun setupFragments(requestCode: Int?) {
        when (requestCode) {
            ChatManager.REQ_CREATE_NEW_GROUP -> {
//                allowMultiple = true
                fragments.add(SelectContactsFragment())
                fragments.add(AddEditGroupDetailsFragment())
            }

            ChatManager.REQ_DIRECT_MESSAGE -> {
//                allowMultiple = false
                fragments.add(SelectContactsFragment())
            }
        }
    }

    override fun getGroupDetails(): GroupDetails {
        return groupDetails
    }

    override fun setGroupDetails(groupDetails: GroupDetails) {
        this.groupDetails = groupDetails
    }

    override fun onNextClicked() {
        var items = pagerAdapter.count
        var position: Int = viewPager.currentItem

        if (position < items - 1) {
            viewPager.currentItem = position + 1
            updateToolbar(viewPager.currentItem)
        } else {
            // Create group in SendBird
            // Go to chat room
            createGroup()

            val intent = Intent()
            intent.putExtra("GROUP_DETAILS", this.groupDetails)
            setResult(Activity.RESULT_OK, intent)
            finish()
        }
    }

    override fun isMultiSelectAllowed(): Boolean? {
        return allowMultiple
    }

    private fun createGroup() {
        val users = groupDetails.users
        val userIds: MutableList<String> = ArrayList()
        for (user in users) {
            Log.i("userId", "inviting " + user.id)
            userIds.add(user.id)
        }

        userIds.add(UserUtil.getUserId()!!)
        val coverPicPath = groupDetails.groupImage

        val groupParams = GroupChannelParams()
        groupParams.setPublic(false)
                .setEphemeral(false)
                .addUserIds(userIds)
                .setName(groupDetails.name)

        // If one-to-one chat with same member,
        // don't create a new group
        Log.i(TAG, "allowMultiple " + allowMultiple.toString())
        groupParams.setDistinct(false)

        if (coverPicPath != null) {
            val filePath = FileStorageUtil.savePictureAndGetFilePath(applicationContext, coverPicPath)
            if (filePath != null) groupParams.setCoverImage(File(filePath))
        }

        GroupChannel.createChannel(groupParams) { groupChannel, e ->
            if (e != null) {
                e.printStackTrace()
                return@createChannel
            }

            goToChatRoom(groupChannel)
        }
    }

    private fun goToChatRoom(groupChannel: GroupChannel) {
        Log.i(TAG, groupChannel.url)
        val intent = Intent(applicationContext, ChatRoomActivity::class.java)
        intent.putExtra("CHANNEL_URL", groupChannel.url)
        intent.putExtra(ChatRoomActivity.contentVideoSTR, contentVideo)
        startActivity(intent)
        finish()
    }

    private fun updateToolbar(currentItem: Int) {
        supportActionBar?.title = when (currentItem) {
            0 -> "Select Contacts"
            1 -> "Group Details"
            else -> "Swingle"
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            onBackPressed()
        }

        return super.onOptionsItemSelected(item)
    }
}

interface CreateGroupListener {

    fun isMultiSelectAllowed(): Boolean?

    fun getGroupDetails(): GroupDetails

    fun setGroupDetails(groupDetails: GroupDetails)

    fun onNextClicked()
}

class MyPagerAdapter(fm: FragmentManager,
                     private var fragments: MutableList<Fragment>) : FragmentPagerAdapter(fm) {

    private val count: Int = fragments.size

    override fun getItem(position: Int): Fragment? {
        return fragments[position]
    }

    override fun getCount(): Int {
        return count
    }
}

class CustomViewPager(context: Context, attributeSet: AttributeSet) :
        ViewPager(context, attributeSet) {

    override fun onInterceptTouchEvent(ev: MotionEvent?): Boolean {
        return false
    }

    override fun onTouchEvent(ev: MotionEvent?): Boolean {
        return false
    }
}