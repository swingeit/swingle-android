package info.swingleit.app.chat

import android.net.Uri
import android.os.Parcel
import android.os.Parcelable
import info.swingleit.app.data.MyAuthor

class GroupDetails() : Parcelable {
    var name: String = ""
    var users: MutableList<MyAuthor> = ArrayList()
    var groupImage: Uri = Uri.EMPTY

    constructor(parcel: Parcel) : this() {
        name = parcel.readString()
        groupImage = parcel.readParcelable(Uri::class.java.classLoader)
    }

    constructor(name: String, users: MutableList<MyAuthor>, groupImage: Uri) : this() {
        this.name = name
        this.users = users
        this.groupImage = groupImage
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeParcelable(groupImage, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<GroupDetails> {
        override fun createFromParcel(parcel: Parcel): GroupDetails {
            return GroupDetails(parcel)
        }

        override fun newArray(size: Int): Array<GroupDetails?> {
            return arrayOfNulls(size)
        }
    }


}