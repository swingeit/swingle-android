package info.swingleit.app.chat

import android.app.Activity
import android.content.Intent
import info.swingleit.app.data.ContentVideo

class ChatManager {
    companion object {
        val REQ_DIRECT_MESSAGE: Int = 391
        val REQ_CREATE_NEW_GROUP: Int = 432
    }

    fun sendDirectMessage(activity: Activity,  contentVideo: ContentVideo? = null) {
        val intent = Intent(activity, CreateGroupActivity::class.java)
        intent.putExtra("ALLOW_MULTIPLE", false)
        intent.putExtra("REQ_CODE", REQ_DIRECT_MESSAGE)
        intent.putExtra(CreateGroupActivity.contentVideoSTR, contentVideo)
        activity.startActivityForResult(intent, REQ_DIRECT_MESSAGE)
        activity.finish()

        // 1 start activity to show contacts
        // allow one or select multiple
        // on selected, close contacts and go to Chat Activity
    }

    fun createNewGroup(activity: Activity, contentVideo: ContentVideo? = null) {
        val intent = Intent(activity, CreateGroupActivity::class.java)
        intent.putExtra("REQ_CODE", REQ_CREATE_NEW_GROUP)
        intent.putExtra("ALLOW_MULTIPLE", true)
        intent.putExtra(CreateGroupActivity.contentVideoSTR, contentVideo)
        activity.startActivityForResult(intent, REQ_CREATE_NEW_GROUP)
        activity.finish()
    }

    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        // TODO
    }
}