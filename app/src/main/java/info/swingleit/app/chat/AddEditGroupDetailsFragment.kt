package info.swingleit.app.chat

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.nononsenseapps.filepicker.FilePickerActivity
import info.swingleit.app.R
import kotlinx.android.synthetic.main.fragment_add_edit_group_details.*


class AddEditGroupDetailsFragment : Fragment() {

    private var listener: CreateGroupListener? = null
    private val READ_REQUEST_CODE: Int = 34232
    val title = "Group Details"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_add_edit_group_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        listener = activity as? CreateGroupActivity

        flGroupPhoto.setOnClickListener {
            showImagePicker()
        }

        tvShareWithEveryone.setOnClickListener {
            var groupDetails = listener?.getGroupDetails()
            val groupName = etGroupName.text.toString().trim()
            if (groupName.isEmpty()) {
                Toast.makeText(context, "set a groupname", Toast.LENGTH_SHORT).show()
            } else {
                groupDetails?.name = groupName
                listener?.onNextClicked()
            }
        }
    }



    private fun showImagePicker() {
        /*
        * FIXME Get Image Picker that gives preview of images
        * */
        Log.i("showImagePicker", "called")

        // This always works
        val i = Intent(context, FilePickerActivity::class.java)
        // This works if you defined the intent filter
        // Intent i = new Intent(Intent.ACTION_GET_CONTENT);

        // Set these depending on your use case. These are the defaults.
        i.putExtra(FilePickerActivity.EXTRA_ALLOW_MULTIPLE, false)
        i.putExtra(FilePickerActivity.EXTRA_ALLOW_CREATE_DIR, false)
        i.putExtra(FilePickerActivity.EXTRA_MODE, FilePickerActivity.MODE_FILE)

        i.putExtra(FilePickerActivity.EXTRA_START_PATH,
                Environment.getExternalStorageDirectory().getPath())

        startActivityForResult(i, READ_REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == READ_REQUEST_CODE
                && resultCode == Activity.RESULT_OK) {
            Log.i("onActivityResult", "got data")
            var uri: Uri? = data?.data
            if (uri != null) {
                Log.i("onActivityResult", "path " + uri.path)
                civGroupPhoto?.setImageURI(uri)
                val groupDetails = listener?.getGroupDetails()!!
                groupDetails.groupImage = uri
                listener?.setGroupDetails(groupDetails)
            }
        }
    }
}
