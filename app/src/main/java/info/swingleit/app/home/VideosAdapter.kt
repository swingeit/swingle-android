package info.swingleit.app.home

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import com.bugfix.broadcastapp.webservice.ApiService
import com.squareup.picasso.Picasso
import info.swingleit.app.R
import info.swingleit.app.data.ContentInteraction
import info.swingleit.app.data.ContentVideo
import info.swingleit.app.sharecontent.ContentShareActivity
import info.swingleit.app.util.AppConstants
import info.swingleit.app.util.DateUtil
import info.swingleit.app.util.UserUtil
import info.swingleit.app.videoplayer.VideoPlayerActivity
import info.swingleit.app.webservice.RestClient
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.item_content_video.view.*
import org.json.JSONObject

class VideosAdapter(private var context: Context,
                    private var items: MutableList<ContentVideo>) : RecyclerView.Adapter<ViewHolder>() {

    var apiService: ApiService = RestClient.getApiService()
    val colorLiked = ContextCompat.getColor(context, R.color.colorLiked)
    val colorNotLiked = ContextCompat.getColor(context, R.color.colorNotLiked)

    private val TAG = "VideosAdapter"

    override fun getItemCount(): Int {
        return items.count()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_content_video, parent, false)
        return ViewHolder(view)
    }

    @SuppressLint("CheckResult")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val contentVideo = items[position]
        contentVideo.isLiked = false
        setupVideo(holder, contentVideo)
        setupClickListeners(holder, contentVideo)
        setupInteractionSubject(holder, contentVideo)
        setupInteractionsDataSubject(holder, contentVideo)
        getInteractions(contentVideo)
    }

    fun setupClickListeners(holder: ViewHolder, contentVideo: ContentVideo) {
        holder.itemView.ivThumbnail.setOnClickListener {
            val intent = Intent(context, VideoPlayerActivity::class.java)
            intent.putExtra(VideoPlayerActivity.videoId, contentVideo.videoId)
            context.startActivity(intent)
        }

        holder.itemView.ivLike.setOnClickListener {
            if (contentVideo.isLiked) {
                unLikeVideoApi(contentVideo)
            } else {
                likeVideoApi(contentVideo)
            }
        }

        holder.itemView.ivShare.setOnClickListener {
            val intent = Intent(context, ContentShareActivity::class.java)
            intent.putExtra(Intent.EXTRA_TEXT, contentVideo.videoUrl)
            context.startActivity(intent)
            shareVideoApi(contentVideo)
        }
    }

    @SuppressLint("CheckResult")
    fun setupInteractionSubject(holder: ViewHolder, contentVideo: ContentVideo) {
        contentVideo.getSubject()
                .subscribe({ contentInteraction ->
                    Log.i(TAG, "content_interaction_type " + contentInteraction.type)

                    if (contentInteraction.type == ContentInteraction.TYPE_SHARE) {
                        contentVideo.shares++
                        shareVideoUi(holder, contentVideo)
                    } else if (contentInteraction.actionType == ContentInteraction.ACTION_TYPE_LIKE) {
                        contentVideo.isLiked = true
                        contentVideo.likes++
                        likeVideoUi(holder, contentVideo)

                    } else {
                        contentVideo.isLiked = false
                        contentVideo.likes--
                        unLikeVideoUi(holder, contentVideo)
                    }

                }, { e ->
                    e.printStackTrace()
                })
    }

    @SuppressLint("CheckResult")
    fun setupInteractionsDataSubject(holder: ViewHolder, contentVideo: ContentVideo) {
        contentVideo.getInteractionsSubject()
                .subscribe({ contentInteractions ->
                    contentInteractions.forEach {
                        Log.i(TAG, "userId_contentInteractions" + it.toMap().toString())
                        if (it.type == ContentInteraction.TYPE_LIKE) {
                            contentVideo.likes++
                        } else {
                            contentVideo.shares++
                        }

                        if (it.userId == UserUtil.getUserId()
                                && it.type == ContentInteraction.TYPE_LIKE) {
                            contentVideo.isLiked = true
                            likeVideoUi(holder, contentVideo)
                        }
                    }

                    holder.itemView.tvLikesCount.text = "${contentVideo.likes}"
                    shareVideoUi(holder, contentVideo)

                }, { e ->
                    e.printStackTrace()
                })
    }

    @SuppressLint("CheckResult")
    fun getInteractions(contentVideo: ContentVideo) {
        // Independent API call
        apiService.getContentInteractions(AppConstants.MONGO_API_KEY,
                getInteractionsQuery(contentVideo.videoId))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    val wow: MutableList<ContentInteraction> = response.body()!!
                    Log.i(TAG, "interactions " + wow.count())
                    wow.forEach {
                        Log.i(TAG, "interactions_map" + it.idMap.toString())
                        Log.i(TAG, "interactions_map" + it.idMap?.get("\$oid"))
                    }
                    contentVideo.saveInteractions(response.body()!!)
                }, { e ->
                    e.printStackTrace()
                })
    }

    @SuppressLint("CheckResult")
    fun likeVideoApi(contentVideo: ContentVideo) {
        var contentInteraction = ContentInteraction(UserUtil.getUserId()!!,
                UserUtil.getUserName()!!,
                contentVideo.videoUrl,
                ContentInteraction.TYPE_LIKE)

        apiService.createContentInteraction(contentInteraction.toMap(), AppConstants.MONGO_API_KEY)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    contentInteraction = response.body()!!
                    contentVideo.like(contentInteraction)
                }, { e ->
                    e.printStackTrace()

                })

    }

    @SuppressLint("CheckResult")
    fun unLikeVideoApi(contentVideo: ContentVideo) {
        var contentInteraction: ContentInteraction? = null

        contentVideo.getSavedInteractions().forEach {
            if (it.videoId == contentVideo.videoId &&
                    it.type == ContentInteraction.TYPE_LIKE) contentInteraction = it
        }

        Log.i(TAG, "interactions_map" + contentInteraction?.idMap.toString())

        apiService.deleteContentInteraction(contentInteraction!!.getId()!!, AppConstants.MONGO_API_KEY)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    contentVideo.unLike(contentInteraction!!)
                }, { e ->
                    e.printStackTrace()
                })
    }

    fun likeVideoUi(holder: ViewHolder, contentVideo: ContentVideo) {
        holder.itemView.ivLike.setColorFilter(colorLiked)
        holder.itemView.tvLikesCount.text = "${contentVideo.likes}"
    }

    fun unLikeVideoUi(holder: ViewHolder, contentVideo: ContentVideo) {
        holder.itemView.ivLike.setColorFilter(colorNotLiked)
        holder.itemView.tvLikesCount.text = "${contentVideo.likes}"
    }

    @SuppressLint("CheckResult")
    fun shareVideoApi(contentVideo: ContentVideo) {
        var contentInteraction = ContentInteraction(UserUtil.getUserId()!!,
                UserUtil.getUserName()!!,
                contentVideo.videoUrl,
                ContentInteraction.TYPE_SHARE)

        apiService.createContentInteraction(contentInteraction.toMap(),
                AppConstants.MONGO_API_KEY)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    contentInteraction = response.body()!!
                    contentVideo.share(contentInteraction)
                }, { e ->
                    e.printStackTrace()

                })
    }

    fun shareVideoUi(holder: ViewHolder, contentVideo: ContentVideo) {
        holder.itemView.tvSharesCount.text = "${contentVideo.shares}"
    }

    private fun setupVideo(holder: ViewHolder, contentVideo: ContentVideo) {
        holder.itemView.tvTitle.text = contentVideo.title
        holder.itemView.tvUploaderName.text = contentVideo.userName
        holder.itemView.tvCreatedAt.text = DateUtil.getRelativeTime(contentVideo.createdAt, context)
        Picasso.get().load(contentVideo.imageUrl).into(holder.itemView.ivThumbnail)
        Picasso.get().load(contentVideo.profileImageUrl).into(holder.itemView.ivUploaderPic)
    }

    fun addItem(contentVideo: ContentVideo) {
        items.add(contentVideo)
        notifyItemInserted(items.count() - 1)
    }

    private fun getInteractionsQuery(videoId: String): MutableMap<String, Any> {
        val interactionFilter = JSONObject()
        interactionFilter.put(ContentInteraction.videoIdSTR, videoId)

        val query: MutableMap<String, Any> = HashMap()
        query["q"] = interactionFilter.toString()
        Log.i("getInteractions videoId", query.toString())
        return query
    }
}