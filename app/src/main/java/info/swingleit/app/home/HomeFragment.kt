package info.swingleit.app.home

import android.app.Fragment
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import info.swingleit.app.R
import info.swingleit.app.util.AppConstants
import info.swingleit.app.util.CategoryManager
import kotlinx.android.synthetic.main.fragment_home.*


/*
* TODO
* 2 - API - Limit videos to Five
* 3 - While posting video and thumbnail url should be posted
* 4 -
* */

class HomeFragment : Fragment() {
    private lateinit var categoriesAdapter: CategoriesAdapter
    private val TAG: String = "HomeFragment"

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.i(TAG, "onViewCreated")
        setupCategories()
    }

    private fun setupCategories() {
        val categories: MutableList<String> = CategoryManager.getAllCategories()
        categoriesAdapter = CategoriesAdapter(activity, categories)
        rvCategory.adapter = categoriesAdapter
        rvCategory.layoutManager = LinearLayoutManager(activity)
        rvCategory.setItemViewCacheSize(25)
    }
}