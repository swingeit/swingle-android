package info.swingleit.app.home

import android.annotation.SuppressLint
import android.app.Fragment
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import com.sendbird.android.SendBird
import com.sendbird.android.User
import info.swingleit.app.BaseActivity
import info.swingleit.app.R
import info.swingleit.app.chatgroups.ChatGroupsFragment
import info.swingleit.app.profile.UserProfileFragment
import info.swingleit.app.util.UserUtil
import kotlinx.android.synthetic.main.activity_home.*
import rebus.permissionutils.PermissionEnum
import rebus.permissionutils.PermissionManager
import rebus.permissionutils.PermissionUtils


/*
* Generate a unique string based on phoneNumber
* */

class HomeActivity : BaseActivity() {

    private val TAG = "HomeActivity"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

//        FIXME firebase remove this
        UserUtil.saveFcmToken("soerenrner")

        fragmentManager.beginTransaction()
                .replace(R.id.flContainer, HomeFragment())
                .commit()

        checkPermissions()
    }

    @SuppressLint("MissingPermission")
    private fun checkPermissions() {
        /*
        * BUG manager!!.line1Number is not return phoneNumber
        * */
        if (PermissionUtils.isGranted(this,
                        PermissionEnum.READ_CONTACTS,
                        PermissionEnum.WRITE_EXTERNAL_STORAGE,
                        PermissionEnum.READ_PHONE_STATE)) {
            setupFragments()
            setupSendBird(UserUtil.getUserId())
        } else {
            PermissionManager.Builder()
                    .permission(PermissionEnum.READ_CONTACTS,
                            PermissionEnum.WRITE_EXTERNAL_STORAGE,
                            PermissionEnum.READ_PHONE_STATE)
                    .askAgain(true)
                    .askAgainCallback { response ->
                        checkPermissions()
                    }
                    .callback { permissionsGranted,
                                permissionsDenied,
                                permissionsDeniedForever,
                                permissionsAsked ->
                        checkPermissions()
                    }
                    .ask(this)
        }
    }

    private fun setupFragments() {
        val homeFragment: Fragment = HomeFragment()
        val chatGroupsFragment: Fragment = ChatGroupsFragment()
        val accountFragment: Fragment = UserProfileFragment()

        bottom_navigation.setOnNavigationItemSelectedListener { item: MenuItem ->
            when (item.itemId) {
                R.id.action_home -> {
                    fragmentManager.beginTransaction()
                            .replace(R.id.flContainer, homeFragment)
                            .commit()
                    true
                }
                R.id.action_chat -> {
                    fragmentManager.beginTransaction()
                            .replace(R.id.flContainer, chatGroupsFragment)
                            .commit()
                    true
                }
                R.id.action_account -> {
                    fragmentManager.beginTransaction()
                            .replace(R.id.flContainer, accountFragment)
                            .commit()
                    true
                }
                else -> {
                    false
                }
            }

        }
    }

    private fun setupSendBird(userId: String?) {
        if (userId == null || userId.isEmpty()) return

        SendBird.connect(userId) { user, e ->
            Log.i("userId", "" + userId)
            if (e != null) {
                e.printStackTrace()
                return@connect
            }

            SendBird.setChannelInvitationPreference(true) { e ->
                if (e != null) {
                    e.printStackTrace()
                }
            }

            setupUserMeta()
        }
    }

    private fun setupUserMeta() {
        val user: User = SendBird.getCurrentUser()
        UserUtil.saveProfileImageUrl(user.profileUrl)

        val data: MutableMap<String, String> = HashMap()
        data["phone"] = UserUtil.getUserId()!!
        data["name"] = UserUtil.getUserName()!!

        user.updateMetaData(data) { u, e ->
            if (e != null) {
                Log.i("user", "create meta failed")
                e.printStackTrace()
            }

            Log.i("user", "create meta success")
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>,
                                            grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        PermissionManager.handleResult(this, requestCode, permissions, grantResults)
    }
}

