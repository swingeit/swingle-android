package info.swingleit.app.home

import android.annotation.SuppressLint
import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import info.swingleit.app.R
import info.swingleit.app.util.AppConstants
import info.swingleit.app.webservice.RestClient
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.item_category_rv.view.*
import org.json.JSONObject

class CategoriesAdapter(private var context: Context,
                        private var items: MutableList<String>) : RecyclerView.Adapter<ViewHolder>() {

    private val TAG: String = "Categories"

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_category_rv, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return items.count()
    }

    @SuppressLint("CheckResult")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        Log.i(TAG, "onBindViewHolder")
        val category = items[position]
        val adapter = VideosAdapter(context, ArrayList())

        holder.itemView.tvCategory.text = category
        holder.itemView.rvVideos.adapter = adapter
        holder.itemView.rvVideos.setItemViewCacheSize(10)
        holder.itemView.rvVideos.layoutManager =
                LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)

        val apiService = RestClient.getApiService()
        apiService.getVideos(AppConstants.MONGO_API_KEY, getVideosQuery(category))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    response.body()?.forEach {
                        adapter.addItem(it)
                    }
                }, {
                    it.printStackTrace()
                })
    }

    private fun getVideosQuery(category: String): MutableMap<String, Any> {
        val categoryFilter = JSONObject()
        categoryFilter.put("category", category)

        val dateSortDesc = JSONObject()
        dateSortDesc.put("created_at", -1)

        val query: MutableMap<String, Any> = HashMap()
        query["q"] = categoryFilter.toString()
        query["l"] = AppConstants.MAX_VIDEOS_PER_CATEGORY
        query["s"] = dateSortDesc.toString()

        return query
    }
}

