package info.swingleit.app

import android.app.Application
import com.github.tamir7.contacts.Contacts
import com.sendbird.android.SendBird
import info.swingleit.app.util.FileStorageUtil
import info.swingleit.app.util.KeyValueStore

class BaseApplication : Application() {
    private val API_ID = "C741B39F-9278-4BF2-A853-756DF12E15B5"

    override fun onCreate() {
        super.onCreate()
        SendBird.init(API_ID, baseContext)
        Contacts.initialize(this)
        FileStorageUtil.init(applicationContext)
        KeyValueStore().init(applicationContext)
    }
}