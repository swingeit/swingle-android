package info.swingleit.app.chatroom.holders

import android.annotation.SuppressLint
import android.content.Intent
import android.util.Log
import android.view.View
import com.stfalcon.chatkit.messages.MessageHolders
import info.swingleit.app.data.MyMessage
import info.swingleit.app.util.YouTubeHelper
import info.swingleit.app.videoplayer.VideoPlayerActivity
import kotlinx.android.synthetic.main.item_custom_outcoming_video_message.view.*

class InVH(view: View, payload: Any?) :
        MessageHolders.IncomingTextMessageViewHolder<MyMessage>(view, payload) {
    val TAG = "InVH"

    @SuppressLint("CheckResult")
    override fun onBind(message: MyMessage?) {
        super.onBind(message)
        itemView.tvMessage.text = message?.text
        imageLoader.loadImage(itemView.imageView, message?.imageUrl)
        itemView.imageView.setOnClickListener {
            Log.i(TAG, "clicked")
            val intent = Intent(itemView.context, VideoPlayerActivity::class.java)
            intent.putExtra(VideoPlayerActivity.videoId,
                    YouTubeHelper.extractVideoIdFromUrl(message?.videoUrl!!))
            itemView.context.startActivity(intent)
        }
    }
}