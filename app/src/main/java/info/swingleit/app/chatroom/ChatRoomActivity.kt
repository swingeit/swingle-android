package info.swingleit.app.chatroom

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.widget.ImageView
import com.sendbird.android.*
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import com.stfalcon.chatkit.commons.ImageLoader
import com.stfalcon.chatkit.messages.MessageHolders
import com.stfalcon.chatkit.messages.MessagesListAdapter
import info.swingleit.app.BaseActivity
import info.swingleit.app.R
import info.swingleit.app.chatroom.holders.InVH
import info.swingleit.app.chatroom.holders.OutVH
import info.swingleit.app.data.ContentVideo
import info.swingleit.app.data.MyAuthor
import info.swingleit.app.data.MyMessage
import info.swingleit.app.home.HomeActivity
import info.swingleit.app.util.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_chat_room.*
import kotlinx.android.synthetic.main.activity_content_share.*
import java.util.*
import kotlin.collections.ArrayList


class ChatRoomActivity : BaseActivity(), MessageHolders.ContentChecker<MyMessage> {

    private val CONTENT_TYPE_CUSTOM: Byte = 2
    private var channel: GroupChannel? = null
    private var channelUrl: String? = null
    lateinit var adapter: MessagesListAdapter<MyMessage>
    private val TAG = "ChatRoomActivity"

    // TODO Load this user from contact
    private var user = SendBird.getCurrentUser()
    private var curUser: MyAuthor = MyAuthor(UserUtil.getUserId()!!,
            UserUtil.getUserName()!!, user.profileUrl)

    companion object {
        val videoUrl = "videoUrl"
        val contentVideoSTR = "contentVideo"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat_room)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_back)
        setupAdapter()
        setupChannel()
    }

    private fun getVideoAndShare() {
        Log.i(TAG, "getVideoAndShare ")
        val contentVideo: ContentVideo? =
                intent?.extras?.getParcelable(contentVideoSTR)
        if (contentVideo != null) {
            Log.i(TAG, "sendMsgAndUpdateUi " + contentVideo.title)
            Log.i(TAG, "sendMsgAndUpdateUi " + contentVideo.videoUrl)
            sendMsgAndUpdateUi(buildMessage(contentVideo))
        }
    }

    private fun setupChannel() {
        channelUrl = intent?.extras?.getString("CHANNEL_URL")
        GroupChannel.getChannel(channelUrl) { groupChannel, e ->
            if (e != null) {
                e.printStackTrace()
                return@getChannel
            }

            channel = groupChannel
            supportActionBar?.title = channel?.name

            loadPreviousMessages()
            getVideoAndShare()
        }
    }

    private fun setupAdapter() {
        val holders = MessageHolders()
                .registerContentType<MyMessage>(
                        CONTENT_TYPE_CUSTOM,
                        InVH::class.java,
                        R.layout.item_custom_incoming_video_message,
                        OutVH::class.java,
                        R.layout.item_custom_outcoming_video_message,
                        this)

        adapter = MessagesListAdapter<MyMessage>(UserUtil.getUserId(), holders, MyImageLoader())
        messagesList.setAdapter(adapter)
        messagesList.setItemViewCacheSize(25)
    }

    private fun loadPreviousMessages() {
        val prevMessageListQuery =
                channel?.createPreviousMessageListQuery()
        prevMessageListQuery?.load(30, true
        ) { messages, e ->
            if (e != null) {
                e.printStackTrace()
            } else {
                val myMsgs: MutableList<MyMessage> = ArrayList()

                for (message in messages) {
                    val userMessage = message as? UserMessage
                    val msg = MyMessage.toMessage(userMessage!!)
                    myMsgs.add(msg)

                    Log.i("in_message_type", userMessage!!.customType)
                    Log.i("in_message_data", userMessage!!.data)
                    Log.i("in_message_message", userMessage!!.message)
                    Log.i("in_message_nickname", userMessage!!.sender.nickname)
                }

                adapter.addToEnd(myMsgs, false)
            }

            setupChatRoom()
            markChannelMessagesAsRead()
        }
    }

    private fun setupChatRoom() {
        SendBird.addChannelHandler(channelUrl, object : SendBird.ChannelHandler() {
            override fun onMessageReceived(ch: BaseChannel?, baseMsg: BaseMessage?) {
                var msg = baseMsg as? UserMessage
                Log.i("in_message_type", msg!!.customType)
                Log.i("in_message_data", msg!!.data)
                Log.i("in_message_message", msg!!.message)
                Log.i("in_message_sender", msg!!.sender.profileUrl)
                Log.i("in_message_name", msg!!.sender.nickname)
                adapter.addToStart(MyMessage.toMessage(msg), true)
            }
        })

        msgInput.setInputListener { input ->
            Log.i("chatroom", "send message")
            val msgStr = input.toString().trim()

            if (msgStr.isValidYouTubeUrl()) {
                val youTubeSearch = YouTubeSearch()
                val vidId = String.getYTVideoId(msgStr)!!
                Log.i("chatroom", "send message")

                youTubeSearch.searchVidByIdAsync(vidId)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({ result ->
                            val title = result["title"]!!
                            val duration = DateUtil.parseDuration(result["duration"]!!)
                            val thumbUrl = String.getYTThumbnailUrl(msgStr)!!
                            Log.i(TAG, "thumbUrl " + thumbUrl)

                            val contentVideo = ContentVideo(UserUtil.getUserId()!!,
                                    UserUtil.getUserName()!!,
                                    UserUtil.getFcmToken()!!,
                                    UserUtil.getProfileImageUrl()!!,
                                    title, duration, thumbUrl, videoUrl!!,
                                    "", 0, 0)

                            sendMsgAndUpdateUi(buildMessage(contentVideo))
                        }, { e ->
                            e.printStackTrace()
                        })
            }
            else {
                sendMsgAndUpdateUi(buildMessage(msgStr))
            }
            true
        }
    }

    private fun sendMsgAndUpdateUi(msg: MyMessage) {
        adapter.addToStart(msg, true)
        Log.i(TAG, "to_string" + msg.toString())

        channel?.sendUserMessage("MyMessage",
                msg.toString(),
                "Youtube_Vid") { message, error ->
            if (error != null) {
                error.printStackTrace()
                return@sendUserMessage
            }

            Log.i("out_message_type", message.customType)
            Log.i("out_message_data", message.data)
            Log.i("out_message_message", message.message)
            Log.i("out_message_nickname", message.sender.nickname)
        }
    }

    private fun buildMessage(msgText: String): MyMessage {
        var imageUrl: String? = null
        if (msgText.isValidYouTubeUrl())
            imageUrl = String.getYTThumbnailUrl(msgText)

        return MyMessage(UUID.randomUUID().toString(),
                msgText,
                curUser,
                Date(),
                imageUrl,
                msgText)
    }

    private fun buildMessage(contentVideo: ContentVideo): MyMessage {

        return MyMessage(UUID.randomUUID().toString(),
                contentVideo.title,
                curUser,
                Date(),
                contentVideo.imageUrl,
                contentVideo.videoUrl)
    }

    private fun markChannelMessagesAsRead() {
        val channels: MutableList<String> = ArrayList()
        channels.add(channelUrl!!)
        SendBird.markAsReadWithChannelUrls(channels) { e ->
            e?.printStackTrace()
        }
    }

    override fun hasContentFor(message: MyMessage?, type: Byte): Boolean {
        return message?.imageUrl != null && message?.videoUrl != null
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            onBackPressed()
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        startActivity(Intent(this, HomeActivity::class.java))
        finish()
    }
}

class MyImageLoader : ImageLoader {
    override fun loadImage(imageView: ImageView?, url: String?) {
        Picasso.get().load(url).into(imageView)
    }
}