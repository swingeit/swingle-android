package info.swingleit.app.sharecontent

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.Toast
import com.github.tamir7.contacts.Contact
import com.github.tamir7.contacts.Contacts
import com.sendbird.android.*
import com.squareup.picasso.Picasso
import com.stfalcon.chatkit.commons.ImageLoader
import com.stfalcon.chatkit.dialogs.DialogsListAdapter
import info.swingleit.app.BaseActivity
import info.swingleit.app.R
import info.swingleit.app.chat.ChatManager
import info.swingleit.app.chatroom.ChatRoomActivity
import info.swingleit.app.data.ContentVideo
import info.swingleit.app.data.MyAuthor
import info.swingleit.app.data.MyChatGroup
import info.swingleit.app.data.MyMessage
import info.swingleit.app.home.HomeActivity
import info.swingleit.app.util.*
import info.swingleit.app.webservice.RestClient
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_content_share.*
import java.util.*


class ContentShareActivity : BaseActivity() {

    private val TAG = "ContentShareActivity"
    private val categories = CategoryManager.getAllCategories()
    private var videoUrl: String? = null
    private var contentVideo: ContentVideo? = null
    private lateinit var dialogsListAdapter: DialogsListAdapter<MyChatGroup>
    private lateinit var chatManager: ChatManager
    private lateinit var adapter: CategoryAdapter

    @SuppressLint("CheckResult")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_content_share)
    }

    override fun onResume() {
        super.onResume()
        supportActionBar?.title = "Share Video"
        videoUrl = intent?.extras?.getString(Intent.EXTRA_TEXT)
        setupSpinner()
        setupShareButton()
        fetchVideoInfo()

        SendBird.connect(UserUtil.getUserId()) { user, e ->
            if (e != null) {
                e.printStackTrace()
                return@connect
            }

            setupChat()
            getChatGroups()
        }
    }

    private fun setupChat() {
        val user = SendBird.getCurrentUser()
        Picasso.get().load(user.profileUrl).into(civProfilePic)

        chatManager = ChatManager()
        dialogsListAdapter = DialogsListAdapter<MyChatGroup>(ImageLoader { imageView, url ->
            Picasso.get().load(url).transform(CircleTransform()).into(imageView)
        })
        dialogsListView.setAdapter(dialogsListAdapter)
    }

    private fun getChatGroups() {
        Log.i(TAG, "getChatGroups")
        val channelListQuery = GroupChannel.createMyGroupChannelListQuery()
        channelListQuery.setIncludeEmpty(true)
        channelListQuery.next(GroupChannelListQuery.GroupChannelListQueryResultHandler { list, e ->
            if (e != null) {    // Error.
                e.printStackTrace()
                return@GroupChannelListQueryResultHandler
            }

            val groupChannels: MutableList<MyChatGroup> = ArrayList<MyChatGroup>()

            for (group in list) {
                groupChannels.add(groupChannelToChatGroup(group))
            }

            dialogsListAdapter.addItems(groupChannels)

            dialogsListAdapter.setOnDialogClickListener { chatGroup: MyChatGroup? ->
                val intent = Intent(context, ChatRoomActivity::class.java)
                intent.putExtra("CHANNEL_URL", chatGroup!!.id)
                intent.putExtra(ChatRoomActivity.contentVideoSTR, contentVideo)
                startActivity(intent)
                finish()
            }
        })
    }

    private fun groupChannelToChatGroup(group: GroupChannel): MyChatGroup {
        val myAuthors: MutableList<MyAuthor> = ArrayList()

        for (member in group.members) {
            myAuthors.add(memberToAuthor(member))
        }

        Log.i("coverurl", group.coverUrl)

        return MyChatGroup(group.coverUrl,
                group.unreadMessageCount,
                group.url,
                myAuthors,
                sendBirdMsgToMyMessage(group.lastMessage),
                getGroupName(group))
    }

    private fun getGroupName(group: GroupChannel): String {
        var groupName: String

        if (group.memberCount == 2) {
            var members: MutableList<Member> = group.members

            var filtered = members.filter {
                it.userId != UserUtil.getUserId()
            }

            // Search in contacts
            val phoneNumber = filtered[0].userId
            val q = Contacts.getQuery()
            q.whereEqualTo(Contact.Field.PhoneNumber, phoneNumber)
            val contacts = q.find()

            Log.i("groupmeta", filtered[0].metaData.toString())
            var userName = filtered[0].metaData.get("name")
            if (userName == null) userName = filtered[0].userId

            groupName = when (contacts.size > 0) {
                false -> userName!!
                true -> contacts[0].displayName
            }

        } else {
            groupName = group.name
        }

        return groupName
    }

    private fun memberToAuthor(member: Member): MyAuthor {
        return MyAuthor(member.userId, member.nickname, member.profileUrl)
    }

    private fun sendBirdMsgToMyMessage(message: BaseMessage?): MyMessage? {
        val msg = message as? UserMessage
        val sender: User? = msg?.sender

        if (sender != null) {
            return MyMessage.toMessage(msg)
        } else {
            return null
        }
    }

    @SuppressLint("CheckResult")
    private fun fetchVideoInfo() {
        if (videoUrl == null) {
            Toast.makeText(this, "Invalid video url", Toast.LENGTH_SHORT).show()
            return
        }

        val vidId = String.getYTVideoId(videoUrl)

        if (vidId == null) {
            Toast.makeText(this, "Invalid video url", Toast.LENGTH_SHORT).show()
            return
        }

        val youTubeSearch = YouTubeSearch()
        youTubeSearch.searchVidByIdAsync(vidId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ result ->
                    val title = result["title"]!!
                    val duration = DateUtil.parseDuration(result["duration"]!!)
                    Log.i(TAG, result.toString())

                    tvTitle.text = title
                    val thumbUrl = String.getYTThumbnailUrl(videoUrl)!!
                    Picasso.get().load(thumbUrl).into(ivThumbnail)

                    contentVideo = ContentVideo(UserUtil.getUserId()!!,
                            UserUtil.getUserName()!!,
                            UserUtil.getFcmToken()!!,
                            UserUtil.getProfileImageUrl()!!,
                            title, duration, thumbUrl, videoUrl!!,
                            "", 0, 0)
                }, { e ->
                    e.printStackTrace()
                })
    }

    private fun setupSpinner() {
        adapter = CategoryAdapter(this, categories)
        rvCategory.adapter = adapter
        rvCategory.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
    }

    private fun setupShareButton() {
        val apiService = RestClient.getApiService()

        clShareWithEveryone.setOnClickListener {
            if (adapter.selectedCategory != null) {
                contentVideo?.category = adapter.selectedCategory!!

                apiService.createVideo(contentVideo!!.toMap(), AppConstants.MONGO_API_KEY)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({ response ->
                            Log.i(TAG, "video created " + response.body().toString())
                            Toast.makeText(ContentShareActivity@ this, "Video posted", Toast.LENGTH_SHORT).show()
                            goToHome()
                        }, {
                            it.printStackTrace()
                        })
            } else {
                Toast.makeText(context, "Select a category", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun goToHome() {
        val intent = Intent(this, HomeActivity::class.java)
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        startActivity(intent)
        finish()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_chat, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.action_new_group -> {
                chatManager.createNewGroup(context, contentVideo)
            }

            R.id.action_direct_message -> {
                chatManager.sendDirectMessage(context, contentVideo)
            }
        }

        return super.onOptionsItemSelected(item)
    }
}
