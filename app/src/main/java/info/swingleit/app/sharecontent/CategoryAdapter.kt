package info.swingleit.app.sharecontent

import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Button
import info.swingleit.app.R
import info.swingleit.app.home.ViewHolder
import kotlinx.android.synthetic.main.item_category.view.*

class CategoryAdapter(private var context: Context,
                      private var items: MutableList<String>) : RecyclerView.Adapter<ViewHolder>() {

    private val TAG: String = "CategoryAdapter"

    var selectedCategory: String? = null
    private var selectedChip: Button? = null
    private var selectedPos: Int = -1
    private val selectedBg = ContextCompat.getDrawable(context, R.drawable.bg_selected_chip)
    private val unSelectedBg = ContextCompat.getDrawable(context, R.drawable.bg_unselected_chip)
    private val selectedTextColor = ContextCompat.getColor(context, android.R.color.white)
    private val unSelectedTextColor = ContextCompat.getColor(context, R.color.colorText)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_category, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return items.count()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val curChip = holder.itemView.chipCategory

        curChip.text = items[position]

        if (selectedPos == position) {
            curChip.background = selectedBg
            curChip.setTextColor(selectedTextColor)
        } else {
            curChip.background = unSelectedBg
            curChip.setTextColor(unSelectedTextColor)
        }

        holder.itemView.chipCategory.setOnClickListener { view ->
            Log.i(TAG, "setOnClickListener")

            if (selectedPos == position) {
                Log.i(TAG, "setOnClickListener unselected")
                selectedChip?.background = selectedBg
                selectedChip?.setTextColor(selectedTextColor)
                selectedChip = null
                selectedCategory = null
                selectedPos = -1
            } else {
                Log.i(TAG, "setOnClickListener different selected")
                selectedChip?.background = unSelectedBg
                selectedChip?.setTextColor(unSelectedTextColor)

                selectedChip = view as Button
                selectedPos = position
                selectedCategory = items[position]
                selectedChip?.background = selectedBg
                selectedChip?.setTextColor(selectedTextColor)
            }
        }
    }
}
