package info.swingleit.app.login

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import info.swingleit.app.BaseActivity
import info.swingleit.app.R
import info.swingleit.app.home.HomeActivity
import info.swingleit.app.util.AppConstants
import info.swingleit.app.util.KeyValueStore
import info.swingleit.app.util.UserUtil
import info.swingleit.app.util.getSaltString
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : BaseActivity() {

    private val keyValueStore = KeyValueStore()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
    }

    override fun isSessionActivity(): Boolean = false

    override fun onResume() {
        super.onResume()
        supportActionBar?.title = "Login"

        btnLogin.setOnClickListener {
            val phoneNumber = etPhoneNumber.text.toString().trim()
            val userName = etUserName.text.toString().trim()

            if (isValidPhoneNumber(phoneNumber) && isValidString(userName)) {
                UserUtil.saveUserName(userName)
                UserUtil.saveUserId(phoneNumber)
                startActivity(Intent(applicationContext, HomeActivity::class.java))
                finish()
            } else {
                Toast.makeText(applicationContext,
                        "Phone number is invalid", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun isValidString(input: String): Boolean {
        return !input.isEmpty()
    }

    private fun isValidPhoneNumber(input: String) : Boolean {
        return input.length == 10
    }
}
