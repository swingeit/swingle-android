package info.swingleit.app.chatgroups

import android.app.Fragment
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.*
import com.github.tamir7.contacts.Contact
import com.github.tamir7.contacts.Contacts
import com.sendbird.android.*
import com.squareup.picasso.Picasso
import com.stfalcon.chatkit.commons.ImageLoader
import com.stfalcon.chatkit.dialogs.DialogsListAdapter
import info.swingleit.app.R
import info.swingleit.app.chat.ChatManager
import info.swingleit.app.chatroom.ChatRoomActivity
import info.swingleit.app.data.MyAuthor
import info.swingleit.app.data.MyChatGroup
import info.swingleit.app.data.MyMessage
import info.swingleit.app.util.CircleTransform
import info.swingleit.app.util.UserUtil
import kotlinx.android.synthetic.main.fragment_chat_groups.*


class ChatGroupsFragment : Fragment() {

    private lateinit var dialogsListAdapter: DialogsListAdapter<MyChatGroup>
    private lateinit var chatManager: ChatManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_chat_groups, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onResume() {
        super.onResume()

        chatManager = ChatManager()
        dialogsListAdapter = DialogsListAdapter<MyChatGroup>(ImageLoader { imageView, url ->
            Picasso.get().load(url).transform(CircleTransform()).into(imageView)
        })
        dialogsListView.setAdapter(dialogsListAdapter)
        getChatGroups()
    }

    private fun getChatGroups() {
        val channelListQuery = GroupChannel.createMyGroupChannelListQuery()
        channelListQuery.setIncludeEmpty(true)
        channelListQuery.next(GroupChannelListQuery.GroupChannelListQueryResultHandler { list, e ->
            if (e != null) {    // Error.
                e.printStackTrace()
                return@GroupChannelListQueryResultHandler
            }

            val groupChannels: MutableList<MyChatGroup> = ArrayList<MyChatGroup>()

            for (group in list) {
                groupChannels.add(groupChannelToChatGroup(group))
            }

            dialogsListAdapter.addItems(groupChannels)

            dialogsListAdapter.setOnDialogClickListener { chatGroup: MyChatGroup? ->
                val intent = Intent(activity, ChatRoomActivity::class.java)
                intent.putExtra("CHANNEL_URL", chatGroup!!.id)
                startActivity(intent)
            }
        })
    }

    private fun groupChannelToChatGroup(group: GroupChannel): MyChatGroup {
        val myAuthors: MutableList<MyAuthor> = ArrayList()

        for (member in group.members) {
            myAuthors.add(memberToAuthor(member))
        }

        Log.i("coverurl", group.coverUrl)

        return MyChatGroup(group.coverUrl,
                group.unreadMessageCount,
                group.url,
                myAuthors,
                sendBirdMsgToMyMessage(group.lastMessage),
                getGroupName(group))
    }

    private fun getGroupName(group: GroupChannel): String {
        var groupName: String

        if (group.memberCount == 2) {
            var members: MutableList<Member> = group.members

            var filtered = members.filter {
                it.userId != UserUtil.getUserId()
            }

            // Search in contacts
            val phoneNumber = filtered[0].userId
            val q = Contacts.getQuery()
            q.whereEqualTo(Contact.Field.PhoneNumber, phoneNumber)
            val contacts = q.find()

            Log.i("groupmeta", filtered[0].metaData.toString())
            var userName = filtered[0].metaData.get("name")
            if (userName == null) userName = filtered[0].userId

            groupName = when (contacts.size > 0) {
                false -> userName!!
                true -> contacts[0].displayName
            }

        } else {
            groupName = group.name
        }

        return groupName
    }

    private fun memberToAuthor(member: Member): MyAuthor {
        return MyAuthor(member.userId, member.nickname, member.profileUrl)
    }

    private fun sendBirdMsgToMyMessage(message: BaseMessage?): MyMessage? {
        val msg = message as? UserMessage
        val sender: User? = msg?.sender

        if (sender != null) {
            return MyMessage.toMessage(msg)
        } else {
            return null
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.menu_chat, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.action_new_group -> {
                chatManager.createNewGroup(activity)
            }

            R.id.action_direct_message -> {
                chatManager.sendDirectMessage(activity)
            }
        }

        return super.onOptionsItemSelected(item)
    }
}