package info.swingleit.app.data

import android.os.Parcel
import android.os.Parcelable
import com.stfalcon.chatkit.commons.models.IUser
import org.json.JSONObject

/*
* Id is phoneNumber. Find a better way
* */
class MyAuthor(private var id: String,
               private var name: String,
               private var avatar: String?) : IUser, Parcelable {

    override fun getId(): String {
        return id
    }

    override fun getName(): String {
        return name
    }

    override fun getAvatar(): String? {
        return avatar
    }

    fun toJson(): JSONObject {
        val obj = JSONObject()
        obj.put("id", id)
        obj.put("name", name)
        obj.put("avatar", avatar)
        return obj
    }

    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(name)
        parcel.writeString(avatar)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<MyAuthor> {
        override fun createFromParcel(parcel: Parcel): MyAuthor {
            return MyAuthor(parcel)
        }

        override fun newArray(size: Int): Array<MyAuthor?> {
            return arrayOfNulls(size)
        }

        fun toMyAuthor(obj: JSONObject): MyAuthor {
            return MyAuthor(obj.getString("id"),
                    obj.getString("name"),
                    obj.getString("avatar"))
        }
    }
}