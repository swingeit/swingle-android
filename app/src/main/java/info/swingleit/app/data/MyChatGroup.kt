package info.swingleit.app.data

import com.stfalcon.chatkit.commons.models.IDialog

class MyChatGroup(private var dialogPhoto: String,
                  private var unreadCount: Int,
                  private var id: String,
                  private var users: MutableList<MyAuthor>,
                  private var lastMessage: MyMessage?,
                  private var dialogName: String) : IDialog<MyMessage> {

    override fun getDialogPhoto(): String {
        return dialogPhoto
    }

    override fun getUnreadCount(): Int {
        return unreadCount
    }

    override fun setLastMessage(message: MyMessage) {
        lastMessage = message
    }

    override fun getId(): String {
        return id
    }

    override fun getUsers(): MutableList<MyAuthor> {
        return users
    }

    override fun getLastMessage(): MyMessage? {
        return lastMessage
    }

    override fun getDialogName(): String {
        return dialogName
    }

}