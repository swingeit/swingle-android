package info.swingleit.app.data

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import info.swingleit.app.util.DateUtil
import info.swingleit.app.util.YouTubeHelper

class ContentInteraction(
        @Expose var userId: String,
        @Expose var userName: String,
        @Expose var videoUrl: String,
        @Expose var type: Int) {

    @Expose
    @SerializedName("_id")
    var idMap: Map<String, String>? = null

    fun getId(): String? = idMap?.get("\$oid")

    @Expose
    @SerializedName("created_at")
    var createdAt = DateUtil.getFormattedCurDate()

    @Expose
    var videoId = YouTubeHelper.extractVideoIdFromUrl(videoUrl)!!

    var actionType: Int = -1

    fun toMap(): MutableMap<String, Any> {
        val data: MutableMap<String, Any> = HashMap()
        data[userIdSTR] = userId
        data[userNameSTR] = userName
        data[videoUrlSTR] = videoUrl
        data[videoIdSTR] = videoId
        data[typeStr] = type
        data[createdAtStr] = createdAt
        return data
    }

    companion object {
        val userIdSTR = "userId"
        val userNameSTR = "userName"
        val videoUrlSTR = "videoUrl"
        val createdAtStr = "created_at"
        val typeStr = "type"
        val videoIdSTR = "videoId"
        val TYPE_LIKE = 0
        val TYPE_SHARE = 1
        val ACTION_TYPE_LIKE = 10
        val ACTION_TYPE_UNLIKE = 11
    }
}