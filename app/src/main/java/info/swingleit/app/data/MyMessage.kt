package info.swingleit.app.data

import com.sendbird.android.UserMessage
import com.stfalcon.chatkit.commons.models.MessageContentType
import org.json.JSONObject
import java.util.*

open class MyMessage(private var id: String,
                     private var text: String,
                     private var user: MyAuthor,
                     private var date: Date,
                     var imageUrl: String? = null,
                     var videoUrl: String? = null) : MessageContentType {

    override fun getId(): String {
        return id
    }

    override fun getCreatedAt(): Date {
        return date
    }

    override fun getUser(): MyAuthor {
        return user
    }

    override fun getText(): String {
        return text
    }

    fun toJson(): JSONObject {
        val obj = JSONObject()
        obj.put("id", id)
        obj.put("text", text)
        obj.put("user", user.toJson())
        obj.put("imageUrl", imageUrl)
        obj.put("videoUrl", videoUrl)
        return obj
    }

    override fun toString(): String {
        return toJson().toString()
    }

    companion object {
        fun toMessage(userMessage: UserMessage): MyMessage {
            // FIXME need a better way to
            // convert user message to MyMessage
            val obj = JSONObject(userMessage.data)
            val sender = userMessage?.sender!!
            val user = MyAuthor(sender.userId,
                    sender.nickname,
                    sender.profileUrl)
            var imageUrl: String? = null
            var videoUrl: String? = null

            if (obj!!.has("imageUrl")) {
                imageUrl = obj!!.getString("imageUrl")
            }

            if (obj!!.has("videoUrl")) {
                videoUrl = obj!!.getString("videoUrl")
            }

            return MyMessage(
                    userMessage.messageId.toString(),
                    obj!!.getString("text"),
                    user, Date(userMessage.createdAt),
                    imageUrl,videoUrl
            )
        }
    }
}