package info.swingleit.app.data

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import info.swingleit.app.util.DateUtil
import info.swingleit.app.util.YouTubeHelper
import io.reactivex.subjects.PublishSubject

data class ContentVideo (
        @Expose var userId: String,
        @Expose var userName: String,
        @Expose var fcmToken: String,
        @Expose var profileImageUrl: String,
        @Expose var title: String,
        @Expose var length: String,
        @Expose var imageUrl: String,
        @Expose var videoUrl: String,
        @Expose var category: String,
        var likes: Int,
        var shares: Int): Parcelable {

    @Expose
    @SerializedName("created_at")
    var createdAt = DateUtil.getFormattedCurDate()

    @Expose
    var videoId = YouTubeHelper.extractVideoIdFromUrl(videoUrl)!!

    var isLiked: Boolean = false

    fun toMap(): MutableMap<String, Any> {
        val data: MutableMap<String, Any> = HashMap()
        data[userIdSTR] = userId
        data[userNameSTR] = userName
        data[fcmTokenSTR] = fcmToken
        data[profileImageUrlSTR] = profileImageUrl
        data[titleSTR] = title
        data[lengthSTR] = length
        data[imageUrlSTR] = imageUrl
        data[videoUrlSTR] = videoUrl
        data[videoIdSTR] = videoId
        data[categorySTR] = category
        data[likesSTR] = likes
        data[shareSTR] = shares
        data[createdAtStr] = createdAt
        return data
    }

    private var interactions: MutableList<ContentInteraction> = ArrayList()

    private var publishSubject: PublishSubject<ContentInteraction> = PublishSubject.create()

    private var interactionsPublishSubject: PublishSubject<MutableList<ContentInteraction>> = PublishSubject.create()

    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readInt(),
            parcel.readInt()) {
        createdAt = parcel.readString()
        videoId = parcel.readString()
        isLiked = parcel.readByte() != 0.toByte()
    }

    fun getSavedInteractions(): MutableList<ContentInteraction> {
        if (interactions == null) interactions = ArrayList()
        return interactions
    }

    fun saveInteractions(interactions: MutableList<ContentInteraction>) {
        this.interactions = interactions
        interactionsPublishSubject.onNext(interactions)
    }

    fun getInteractionsSubject(): PublishSubject<MutableList<ContentInteraction>> {
        if (interactionsPublishSubject == null) interactionsPublishSubject = PublishSubject.create()
        return interactionsPublishSubject
    }

    fun getSubject(): PublishSubject<ContentInteraction> {
        if (publishSubject == null) publishSubject = PublishSubject.create()
        return publishSubject
    }

    fun like(contentInteraction: ContentInteraction) {
        var items = getSavedInteractions()
        contentInteraction.actionType = ContentInteraction.ACTION_TYPE_LIKE
        items.add(contentInteraction)
        getSubject().onNext(contentInteraction)
    }

    fun unLike(contentInteraction: ContentInteraction) {
        var items = getSavedInteractions()
        items.remove(contentInteraction)
        contentInteraction.actionType = ContentInteraction.ACTION_TYPE_UNLIKE
        getSubject().onNext(contentInteraction)
    }

    fun share(contentInteraction: ContentInteraction) {
        interactions.add(contentInteraction)
        getSubject().onNext(contentInteraction)
    }

//    companion object {
//        val userIdSTR = "userId"
//        val userNameSTR = "userName"
//        val fcmTokenSTR = "fcmToken"
//        val profileImageUrlSTR = "profileImageUrl"
//        val titleSTR = "title"
//        val lengthSTR = "length"
//        val imageUrlSTR = "imageUrl"
//        val videoUrlSTR = "videoUrl"
//        val videoIdSTR = "videoId"
//        val categorySTR = "category"
//        val likesSTR = "likes"
//        val shareSTR = "shares"
//        val createdAtStr = "created_at"
//    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(userId)
        parcel.writeString(userName)
        parcel.writeString(fcmToken)
        parcel.writeString(profileImageUrl)
        parcel.writeString(title)
        parcel.writeString(length)
        parcel.writeString(imageUrl)
        parcel.writeString(videoUrl)
        parcel.writeString(category)
        parcel.writeInt(likes)
        parcel.writeInt(shares)
        parcel.writeString(createdAt)
        parcel.writeString(videoId)
        parcel.writeByte(if (isLiked) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ContentVideo> {
        override fun createFromParcel(parcel: Parcel): ContentVideo {
            return ContentVideo(parcel)
        }

        override fun newArray(size: Int): Array<ContentVideo?> {
            return arrayOfNulls(size)
        }

        val userIdSTR = "userId"
        val userNameSTR = "userName"
        val fcmTokenSTR = "fcmToken"
        val profileImageUrlSTR = "profileImageUrl"
        val titleSTR = "title"
        val lengthSTR = "length"
        val imageUrlSTR = "imageUrl"
        val videoUrlSTR = "videoUrl"
        val videoIdSTR = "videoId"
        val categorySTR = "category"
        val likesSTR = "likes"
        val shareSTR = "shares"
        val createdAtStr = "created_at"
    }
}