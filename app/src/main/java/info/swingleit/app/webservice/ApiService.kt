package com.bugfix.broadcastapp.webservice

import info.swingleit.app.data.ContentInteraction
import info.swingleit.app.data.ContentVideo
import io.reactivex.Flowable
import retrofit2.Response
import retrofit2.http.*


interface ApiService {

    // Get videos in collection
    @GET("api/1/databases/mongo-backend/collections/videos")
    fun getVideos(@Query("apiKey") apiKey: String,
                  @QueryMap params: MutableMap<String, Any>? = null): Flowable<Response<MutableList<ContentVideo>>>

    // Add a new video in the collection
    @Headers("contentType: application/json")
    @POST("api/1/databases/mongo-backend/collections/videos")
    fun createVideo(@Body data: MutableMap<String, Any>,
                    @Query("apiKey") apiKey: String): Flowable<Response<Any>>

    // Get user interactions in collection
    @GET("api/1/databases/mongo-backend/collections/content_interactions")
    fun getContentInteractions(@Query("apiKey") apiKey: String,
                               @QueryMap params: MutableMap<String, Any>? = null): Flowable<Response<MutableList<ContentInteraction>>>

    // Add a new user interaction in the collection
    @Headers("contentType: application/json")
    @POST("api/1/databases/mongo-backend/collections/content_interactions")
    fun createContentInteraction(@Body data: MutableMap<String, Any>,
                                 @Query("apiKey") apiKey: String): Flowable<Response<ContentInteraction>>

    // Add a remove user interaction in the collection
    @Headers("contentType: application/json")
    @DELETE("api/1/databases/mongo-backend/collections/content_interactions/{id}")
    fun deleteContentInteraction(@Path("id") id: String,
                                 @Query("apiKey") apiKey: String): Flowable<Response<Any>>
}