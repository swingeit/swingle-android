package info.swingleit.app.webservice

import com.bugfix.broadcastapp.webservice.ApiService
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import info.swingleit.app.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class RestClient {

    companion object {
        private const val API_BASE_URL = "https://api.mlab.com/"

        private val okHttpClient = OkHttpClient.Builder()
                .addInterceptor(getBodyLoggerInterceptor())
                .addInterceptor(getHeaderLoggerInterceptor())
                .build()

        private val gson = GsonBuilder().excludeFieldsWithoutExposeAnnotation().create()

        private val retrofit = Retrofit.Builder()
                .baseUrl(API_BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(okHttpClient)
                .build()

        private val apiService = retrofit.create(ApiService::class.java)

        fun getApiService(): ApiService {
            return apiService
        }

        /*
        * https://stackoverflow.com/questions/39784243/cant-resolve-setlevel-on-httplogginginterceptor-retrofit2-0
        * */
        private fun getBodyLoggerInterceptor(): HttpLoggingInterceptor {
            val logging = HttpLoggingInterceptor()
            if (BuildConfig.DEBUG) {
                logging.level = HttpLoggingInterceptor.Level.BODY
            }
            return logging
        }

        private fun getHeaderLoggerInterceptor(): HttpLoggingInterceptor {
            val logging = HttpLoggingInterceptor()
            if (BuildConfig.DEBUG) {
                logging.level = HttpLoggingInterceptor.Level.BODY
            }
            return logging
        }
    }
}

